# Video recorder service API

An web service for recording selenium browser services using Nodejs, Express and FFMPEG

## Endpoints

```REST
POST /start   Start recording

POST /stop    Stop recording and retrieve file
```