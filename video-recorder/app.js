const fs = require('fs');
const {unlink} = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const ffmpeg = require('fluent-ffmpeg');
const winston = require('winston');
const {v4: uuidv4} = require("uuid");

const {format, createLogger} = winston;
const {colorize, simple} = format;

const console = new winston.transports.Console({
    level: 'info',
    format: winston.format.combine(
      colorize(),
      simple()
    )
  });
const file = new winston.transports.File({
    level: 'info',
    filename: 'combined.log',
    format: simple()
  });

const transports = [console, file];
const log = createLogger({transports});

let recordings = {};

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Delay xxx milliseconds
const delay = ms => new Promise(res => setTimeout(res, ms));

// Check if file exists
const fileExists = async (recording, times) => {
  while (!recording.finished && times > 0) {
    await delay(50);
    times--;
  }

  if (recording.finished) {
    log.info(`File ${recording.path} exists!`);
    return "File exists";
  } else {
    log.error(`File ${recording.path} doesn't exist!`);
    throw new Error("File doesn't exist");
  }
};

// Check for minimal FPS needed
const fpsNeeded = async (progress, fps, recording, times) => {
  while (progress.currentFps < fps && recording.error === undefined && times > 0) {
    await delay(50);
    times--;
  }

  if (progress.currentFps >= fps) {
    return "Ok";
  } else if (recording.error) {
    throw recording.error;
  } else {
    log.warn(`Minimum fps needed (${fps}) were not available\n`);  }
};

// Stop stream
function stopStream(stream) {
  stream.ffmpegProc.stdin.write('q');
}

function clearLog() {
  // Empty log
  fs.writeFile('combined.log', '', () => null);
}

function showLog() {
  // Show log data
  return `
  =======
  | Log |
  =======
  
  ${fs.readFileSync('combined.log', 'utf8')}`;
}

app.post("/start", async function (req, res) {
  // Empty log
  clearLog();

  // Start recording
  const {source, size, fps, pixelFormat, fileFormat, extraInput, extraOutput} = req.body;
  let recordingId = uuidv4();
  let recordPath = `${__dirname}/records/${recordingId}${fileFormat}`;
  let currentProgress = {currentFps: 0};
  let inputOptions = [
    '-f x11grab',
    `-video_size ${size || "1280x1024"}`,
    `-framerate ${fps || "24"}`,
    `-draw_mouse 0`,
    ...(extraInput || [])
  ];

  let outputOptions = [
    `-pix_fmt ${pixelFormat || "yuv420p"}`,
    ...(extraOutput || [])
  ];

  // Log input options
  log.info(`Options are "${[...inputOptions, ...outputOptions].join(" ")}"`);

  try {
    recordings[recordingId] = {
      path: recordPath,
      options: [...inputOptions, ...outputOptions],
      recording: ffmpeg({source: source, logger: log})
        .inputOptions(inputOptions)
        .outputOptions(outputOptions)
        .output(recordPath)
        .on('end', function () {
          log.info(`File recording has ended: ${recordingId}`);
          recordings[recordingId].finished = true;
        })
        .on('progress', function (progress) {
          log.info(`Processing: ${progress.currentKbps} kbps @ ${progress.currentFps} fps`);
          currentProgress.currentFps = progress.currentFps;
        })
        .on('error', function (error) {
          log.error(`Error recording file ${recordingId}\n${error.stack}`);
          recordings[recordingId].error = error;
        })
    }

    // Log recording
    log.info(`Start recording ${recordingId}`);

    // Start recording
    recordings[recordingId].recording.run();

    // Await fps starts
    await fpsNeeded(currentProgress, 1, recordings[recordingId], 200);

    // Retrieve recording id
    res.send(recordingId);
  } catch (err) {
    res.status(500).send(`Error @ start recording, ${err}${showLog()}`);
  }
});

app.post("/stop", async (req, res, next) => {
  let recordingId = req.body.id;
  const {recording, error, path, options} = recordings[recordingId];

  if (error) {
    res.status(500).send(`Error recording file ${recordingId} - ${error} - Input options: ${options}`);
    next();
  } else {
    try {
      // Log recording
      log.info(`Stop recording ${recordingId}`);

      // Stop recording
      stopStream(recording);

      // Retrieve recording when it exists
      await fileExists(recordings[recordingId], 600);

      // Download file
      res
        .download(path, errDownload => {
          if (errDownload) res.status(500).send(`Error downloading file, ${errDownload}${showLog()}`);
          // Delete file after download
          unlink(path, errDelete => {
            if (errDelete) res.status(500).send(`Error deleting file, ${errDelete}${showLog()}`);
            log.info(`Successfully deleted ${path}`);
          });
        });

      // Delete recording
      delete recordings[recordingId];
    } catch (err) {
      res.status(500).send(`Error @ stop recording: ${recordingId}. Error is ${err}${showLog()}`);
      next();
    }
  }
});

app.post("/log", (req, res) => {
  res.send(fs.readFileSync('combined.log', 'utf8'));
});

const server = app.listen(3000, function () {
  let port = server.address().port;
  log.info(`Server initialized on port ${port}`);
});

app.use(function (req, res) {
  res.status(404).send("Route not available");
});